#!/usr/bin/python2.7
#
# Interface for the assignement
#

# Assignment 1 - Poojita Badarinarayan - 1211235087

import psycopg2
import sys
import StringIO

DATABASE_NAME = 'dds_assgn1'

def getopenconnection(user='postgres', password='1234', dbname='dds_assgn1'):
    return psycopg2.connect("dbname='" + dbname + "' user='" + user + "' host='localhost' password='" + password + "'")



def loadratings(ratingstablename, ratingsfilepath, con):

        fread = open(ratingsfilepath, 'r')

        if fread:
            data = fread.read().replace('::', '\t').replace('\r', '')

            f_data = StringIO.StringIO()
            f_data.write(data)
            f_data.seek(0)

            cur = con.cursor()

            cur.execute('DROP TABLE IF EXISTS %s' % (ratingstablename,))
            cur.execute(
                'CREATE TABLE %s (UserID integer, MovieID integer, Rating float, Other integer)' % (ratingstablename,))
            cur.copy_from(f_data, ratingstablename)
            cur.execute('ALTER TABLE %s DROP COLUMN Other' % (ratingstablename,))

            con.commit()

            f_data.close()
            fread.close()

        pass



def rangepartition(ratingstablename, numberofpartitions, openconnection):

    dividing_value = float (5.0/int(numberofpartitions))
    con = openconnection
    cur = con.cursor()

    i = 0
    p = 0.0
    q = 0

    if dividing_value > 0:

        cur.execute('DROP TABLE IF EXISTS META_RANGE')
        #META_RANGE - Metatable which consists information on the range partition table names, Upper & Lower Range values
        cur.execute('CREATE TABLE META_RANGE (TableName varchar, LowerRating float, UpperRating float)')
        while q <= 5.0:
            table_name = "range_part" + str(i)
            cur.execute('DROP TABLE IF EXISTS %s' % (table_name,))

            if q==0:
                q = dividing_value
                #Creating range table partitions by considering the 1st partition which is inclusive of both ranges
                cur.execute(
                    'CREATE TABLE range_part' + str(i) + ' AS SELECT * FROM ' + ratingstablename + ' WHERE Rating >=' + str(
                        p) + ' AND Rating <=' + str(q))

                lowerRange = p
                upperRange = q

                if lowerRange == 0:
                    lowerRange = -1
                #Storing the range partition table's info in meta_range
                cur.execute('INSERT INTO META_RANGE ( TableName, LowerRating, UpperRating) VALUES (%s, %s, %s)',(str(table_name), lowerRange, upperRange,))
                p = q
                q += dividing_value
                i+=1
            else:
                #Creating range partion tables for rest of the cases wherein it is exclusive of upper range & inclusive of lower range
                cur.execute(
                    'CREATE TABLE range_part' + str(
                        i) + ' AS SELECT * FROM ' + ratingstablename + ' WHERE Rating >' + str(
                        p) + ' AND Rating <=' + str(q))
                cur.execute('INSERT INTO META_RANGE ( TableName, LowerRating, UpperRating) VALUES (%s, %s, %s)',
                            (str(table_name), p, q,))
                p = q
                q += dividing_value
                i += 1


    con.commit()

    pass


def roundrobinpartition(ratingstablename, numberofpartitions, openconnection):
    con = openconnection
    cur = con.cursor()
    i = 0


    cur.execute('DROP TABLE IF EXISTS META_ROBIN')
    #META_ROBIN - metatable which consists information of round-robin partioned table names & number of rows in each of it
    cur.execute('CREATE TABLE META_ROBIN (TableName varchar, NumberOfRows integer)')
    for i in range(0, int(numberofpartitions)):
        print i
        tablename = "rrobin_part" +str(i)
        cur.execute('DROP TABLE IF EXISTS %s' % (tablename,))
        #Create round robin partition tables using a comparison of (row_number % numberofpartions) = ((i+1) % numberofpartition)
        #Reference: http://www.postgresqltutorial.com/postgresql-row_number/
        sql_query = 'CREATE TABLE rrobin_part' + str(i) + ' AS SELECT * FROM ( SELECT UserID, MovieID, Rating, ROW_NUMBER() OVER() FROM ' +str(ratingstablename)+  ' ) x WHERE ROW_NUMBER % ' +str(numberofpartitions)+ ' = ' +str(int((i+1)% int(numberofpartitions)))
        cur.execute(sql_query)
        #Deleting an extra column comprising of row number & converting it to required schema's format
        cur.execute( 'ALTER TABLE %s DROP COLUMN ROW_NUMBER' % (tablename, ))
        cur.execute('SELECT COUNT (*) FROM ' + tablename)
        count = cur.fetchone()[0]
        #Capturing the round-robin partition's table information in metatable
        cur.execute('INSERT INTO META_ROBIN ( TableName, NumberOfRows) VALUES (%s, %s)',(str(tablename), count))
        con.commit()


    con.commit()
    pass


def roundrobininsert(ratingstablename, userid, itemid, rating, openconnection):
    con = openconnection
    cur = con.cursor()

    cur.execute('SELECT SUM(NumberOfRows) FROM META_ROBIN')
    count = cur.fetchone()[0]
    cur.execute('SELECT COUNT (*) FROM META_ROBIN')
    No_of_partitions = cur.fetchone()[0]
    #Extra row addition is represented by count + 1
    idx = int((count+1)%(No_of_partitions))

    if (idx == 0):
        idx = No_of_partitions - 1
    else:
        idx -= 1

    tablename = "rrobin_part" + str(idx)
    #Inserting into Required table using idx
    sql_query = 'INSERT INTO ' + str(tablename) + ' (UserID, MovieID, Rating) VALUES(' + str(userid) + ',' + str(itemid) + ',' + str(rating) + ')'
    cur.execute(sql_query)
    #Addition of extra row is updated in meta table
    cur.execute('UPDATE META_ROBIN SET NumberOfRows=(NumberOfRows + 1) WHERE TableName=\'%s\'' % (tablename,))

    con.commit()
    pass


def rangeinsert(ratingstablename, userid, itemid, rating, openconnection):

    con = openconnection
    cur = con.cursor()
    cur.execute('SELECT TableName FROM META_RANGE WHERE '+str(rating)+'> LowerRating AND '+str(rating)+'<= UpperRating')
    table_name = cur.fetchone()[0]
    #Inserting the new record in required table using rating attribute
    sql_query= 'INSERT INTO ' +str(table_name)+ ' (UserID, MovieID, Rating) VALUES('+str(userid)+','+str(itemid)+','+str(rating)+')'
    cur.execute(sql_query)
    con.commit()
    pass


def create_db(dbname):
    """
    We create a DB by connecting to the default user and database of Postgres
    The function first checks if an existing database exists for a given name, else creates it.
    :return:None
    """
    # Connect to the default database
    con = getopenconnection(dbname='postgres')
    con.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()

    # Check if an existing database with the same name exists
    cur.execute('SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname=\'%s\'' % (dbname,))
    count = cur.fetchone()[0]
    if count == 0:
        cur.execute('CREATE DATABASE %s' % (dbname,))  # Create the database
    else:
        print 'A database named {0} already exists'.format(dbname)

    # Clean up
    cur.close()
    con.close()


def deletepartitionsandexit(openconnection):
    con = openconnection
    cur = con.cursor()

    #Deleting all tables listed in META_RANGE & deleting the META_RANGE table
    try:

        cur.execute('SELECT TableName FROM META_RANGE')
        rows = cur.fetchall()
        for row in rows:
            sql = 'DROP TABLE IF EXISTS '+str(row[0])
            cur.execute(sql)
        cur.execute('DROP TABLE IF EXISTS META_RANGE')
        con.commit()

    except Exception:

        print "No Range partitions created"
    # Deleting all tables listed in META_ROBIN & deleting the META_ROBIN table
    try:

        cur.execute('SELECT TableName FROM META_ROBIN')
        rows = cur.fetchall()
        for row in rows:
            sql = 'DROP TABLE IF EXISTS ' + str(row[0])
            cur.execute(sql)
        cur.execute('DROP TABLE IF EXISTS META_ROBIN')
        con.commit()

    except Exception:

        print "No Round-Robin Partitions created"


    pass

# Middleware
def before_db_creation_middleware():
    pass


def after_db_creation_middleware(databasename):
    # Use it if you want to
    pass


def before_test_script_starts_middleware(openconnection, databasename):
    # Use it if you want to
    pass


def after_test_script_ends_middleware(openconnection, databasename):
    # Use it if you want to
    pass


if __name__ == '__main__':
    try:

        # Use this function to do any set up before creating the DB, if any
        before_db_creation_middleware()

        create_db(DATABASE_NAME)

        # Use this function to do any set up after creating the DB, if any
        after_db_creation_middleware(DATABASE_NAME)

        ratings_file_path = None

        #Collecting the file path of Ratings.dat & The number of partitions as command line argument
        if len(sys.argv) == 3:
            ratings_file_path = sys.argv[1]
            N = sys.argv[2]

        print "Value of N is", N

        with getopenconnection() as con:
            # Use this function to do any set up before I starting calling your functions to test, if you want to
            before_test_script_starts_middleware(con, DATABASE_NAME)

            # Here is where I will start calling your functions to test them. For example,

            loadratings('Ratings', ratings_file_path, con)
            rangepartition('Ratings', N, con)
            roundrobinpartition('Ratings', N, con)
            # rangeinsert('Ratings', 100, 2, 3, con)
            # roundrobininsert('Ratings',100, 1, 3, con)
            #deletepartitionsandexit(con)


            # ###################################################################################
            # Anything in this area will not be executed as I will call your functions directly
            # so please add whatever code you want to add in main, in the middleware functions provided "only"
            # ###################################################################################

            # Use this function to do any set up after I finish testing, if you want to
            after_test_script_ends_middleware(con, DATABASE_NAME)

    except Exception as detail:
        print "OOPS! This is the error ==> ", detail
