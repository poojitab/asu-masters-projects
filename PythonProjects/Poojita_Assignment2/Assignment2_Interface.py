#!/usr/bin/python2.7
#
# Assignment2 Interface

####    Poojita Badarinarayan - 1211235087  ####


import psycopg2
import os
import sys
# Donot close the connection inside this file i.e. do not perform openconnection.close()
def RangeQuery(ratingsTableName, ratingMinValue, ratingMaxValue, openconnection):

    cur = openconnection.cursor()
    outFile = open("RangeQueryOut.txt", "w")

    cur.execute("SELECT PartitionNum FROM RoundRobinRatingsMetadata")
    rrobinPartitionNum = cur.fetchall()[0][0]
    #Traversing through all  round-robin partitions
    # To find the tuples with rating value between ratingMinValue & ratingMaxValue both inclusive
    for i in range(0, int(rrobinPartitionNum)):
        rrobinTableName = "RoundRobinRatingsPart" + str(i)
        cur.execute("SELECT * from " + rrobinTableName + " WHERE Rating >= " + str(ratingMinValue) + " AND Rating <= " + str(ratingMaxValue))
        count = cur.rowcount
        if count > 0:
            rows = cur.fetchall()
            for row in rows:
                outFile.write(rrobinTableName + "," + str(row[0]) + "," + str(row[1]) + "," + str(row[2]))
                outFile.write("\n")
    #Find an immediate lower value (tempMin) of ratingMinValue in MinRating column & immediate upper value (tempMax) of ratingMax value in MaxRating column
    # In RangeRatings Meta Table
    #Find all partitions with MinRating > = tempMin intersecting partitions with Max <= tempMax
    #Look for required ratings value tuples in these partitions
    tempMin = 0
    if ratingMinValue !=0:
       cur.execute("SELECT MinRating FROM RangeRatingsMetadata WHERE MinRating < "+str(ratingMinValue)+" ORDER BY MinRating DESC")
       tempMin = cur.fetchall()[0][0]

    cur.execute("SELECT MaxRating FROM RangeRatingsMetadata WHERE MaxRating >= "+str(ratingMaxValue)+" ORDER BY MaxRating ASC")
    tempMax = cur.fetchall()[0][0]

    cur.execute("SELECT PartitionNum FROM RangeRatingsMetadata WHERE MinRating >= "+str(tempMin)+" INTERSECT SELECT PartitionNum FROM RangeRatingsMetadata WHERE MaxRating <= "+str(tempMax)+" ORDER BY PartitionNum ASC")
    range_table_suffixNames = cur.fetchall()
    for range_table_suffix in range_table_suffixNames:
        rangeTableName = "RangeRatingsPart"+str(range_table_suffix[0])
        cur.execute("SELECT * FROM "+rangeTableName)
        rows = cur.fetchall()
        for row in rows:
           outFile.write(rangeTableName+","+str(row[0])+","+str(row[1])+","+str(row[2]))
           outFile.write("\n")

    outFile.close()





def PointQuery(ratingsTableName, ratingValue, openconnection):
    cur = openconnection.cursor()
    outFile = open("PointQueryOut.txt", "w")

    cur.execute("SELECT PartitionNum FROM RoundRobinRatingsMetadata")
    rrobinPartitionNum = cur.fetchall()[0][0]
    # Obtain number of partitions from RoundRobinRatingsMetaData
    #Traverse through all partitions to find the required rating value tuples
    for i in range(0, int(rrobinPartitionNum)):
        rrobinTableName = "RoundRobinRatingsPart" + str(i)
        query = "SELECT * FROM %s WHERE Rating::numeric = %f" % (rrobinTableName, ratingValue)
        cur.execute(query)
        if cur.rowcount > 0:
            rows = cur.fetchall()
            for row in rows:
                outFile.write(rrobinTableName + "," + str(row[0]) + "," + str(row[1]) + "," + str(row[2]))
                outFile.write("\n")

    #Select an immediate greater value of ratingsValue in Max Rating column of RangeRatingsMetadata table
    #Select the partition number corresponding to that MaxRating
    #Look for required rating value tuples in this partition
    cur.execute("SELECT MaxRating FROM RangeRatingsMetadata WHERE MaxRating >= "+str(ratingValue)+" ORDER BY MaxRating ASC")
    maxVal = cur.fetchall()[0][0]

    cur.execute("SELECT PartitionNum FROM RangeRatingsMetadata WHERE MaxRating = "+str(maxVal))
    range_table_suffixNames = cur.fetchall()
    for range_table_suffix in range_table_suffixNames:
         rangeTableName = "RangeRatingsPart" + str(range_table_suffix[0])
         cur.execute("SELECT * FROM " + rangeTableName + " WHERE Rating::numeric = %f" % (ratingValue))
         rows = cur.fetchall()
         for row in rows:
             outFile.write(rangeTableName + "," + str(row[0]) + "," + str(row[1]) + "," + str(row[2]))
             outFile.write("\n")


    outFile.close()
