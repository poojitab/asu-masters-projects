CSE 591:Adaptive Web - Assignment 1
Poojita Badarinarayan


Components used in the assignment:

For the server & client set up
Flask (with Python 2.7)
SqlAlchemy (Database)
HTML with CSS for UI 

For Chrome Extension
Json (Manifest.json)
Javascript(contentscript.js)

Dependencies
flask,sqlalchemy modules ('pip install' each of them in python 2.7 folder)

Events Captured:
-Upvote
-Downvote
-Scroll
-Post an answer
-Ask a question
-Add to favorite tag(Bonus)
-Share the interested tag(Bonus)
-Job search area clicked(Bonus)

How to run the code:
1)Run app.py
2)Go to chrome browser & use the url http://localhost:4000
3)This leads us to a login page. If you already have an account please put in your credentials to login to the system. Otherwise please use the signup link in the page to create an account
4) Once we get into the system (the user profile page),content of which events & why they are logged in, login history, link to stackoverflow (tag=java), logout option can be found
5)To visit the stackoverflow link from a particular user, please click the link provided in user page
6)In stackoverflow page please use the shield button & click load unsafe scripts - VERY IMPORTANT (info about this provided in user profile page too)
7)Perform  actions scroll up/down, post an answer, ask a question, upvote, downvote, add a favorite tag, share the tag,job search area click(on the right side). These events are captured in a file for each user's session. When a user logs in there is also a table available on user profile page showing all his/her behavior interaction filenames created so far with time stamps. {The events are captured using chrome extension)
8)After events are performed please click the logout button - VERY IMPORTANT (As sessionwise info is captured)

-The chrome extension implemented in this assignment (in folder "chrome-ext" for behavior capturing is attached to the zip file.)
-A video recording(Poojita_Assignment1) depicting how the system works can be found here https://www.youtube.com/watch?v=1Y5hnIRFGHA&feature=youtu.be
-There are also logs previously collected for users aaa, bbb, ccc ((as files -persistent data)  with all events in the folder cse591a1.

References
[1]https://pythonspot.com/en/login-authentication-with-flask/
[2]https://developer.chrome.com/extensions/getstarted