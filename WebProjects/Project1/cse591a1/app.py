from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort
import os, datetime, uuid
from sqlalchemy.orm import sessionmaker
from database import *

engine = create_engine('sqlite:///tutorial.db', echo=True)

app = Flask(__name__)

logFileHandle = null

@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        Session = sessionmaker(bind=engine)
        s = Session()
        query1 = s.query(SessionInfo).filter(SessionInfo.username.in_([session.get('current_user')]))
        sessionInfoItems = query1.all()

        query2 = s.query(EventInfo).filter(EventInfo.username.in_([session.get('current_user')]))
        eventInfoItems = query2.all()

        return render_template('userprofile.html',sessionInfoItems=sessionInfoItems, eventInfoItems=eventInfoItems)


@app.route('/login', methods=['POST'])
def do_admin_login():
    POST_USERNAME = str(request.form['username'])
    POST_PASSWORD = str(request.form['password'])
    Session = sessionmaker(bind=engine)
    s = Session()
    query = s.query(User).filter(User.username.in_([POST_USERNAME]), User.password.in_([POST_PASSWORD]))
    result = query.first()
    if result:
        global logFileHandle
        session['logged_in'] = True
        session['current_user'] = POST_USERNAME
        session['login_time'] = datetime.datetime.now()
        logFileName = POST_USERNAME+"_"+str(uuid.uuid4().hex)+".txt"
        session['eventfilename']  = logFileName
        logFileHandle = open(logFileName, 'w')
    else:
        flash('wrong password!or user does not exist!')
    return home()

@app.route('/signup', methods=['GET', 'POST'])
def do_register_user():
    session['user_creation_status_message'] = ""
    if request.method == 'POST':
        POST_USERNAME = str(request.form['username'])
        POST_PASSWORD = str(request.form['password'])
        POST_CONFIRM_PASSWORD = str(request.form['confirm-password'])
        if POST_USERNAME and POST_PASSWORD and POST_CONFIRM_PASSWORD:
            if POST_PASSWORD == POST_CONFIRM_PASSWORD:
                Session = sessionmaker(bind=engine)
                s = Session()
                query = s.query(User).filter(User.username.in_([POST_USERNAME]))
                result = query.first()
                if result:
                    session['user_creation_status_message'] = 'Sorry user already exists ! Please Retry'
                else:
                    userObj = User(POST_USERNAME, POST_PASSWORD)
                    s.add(userObj)
                    s.commit()
                    session['user_creation_status_message'] = 'User created successfully ! Please proceed to login page'
            else:
                session['user_creation_status_message'] = 'Passwords do not match! Please Retry'
        else:
            session['user_creation_status_message'] = 'Missing fields. All fields are mandatory!'

    return render_template('signup.html')


@app.route('/logdata', methods=['POST'])
def do_log_data():
    data = str(request.get_data())
    msg = str(datetime.datetime.now()) + "       "+data+os.linesep
    global logFileHandle
    if logFileHandle and not logFileHandle.closed:
        logFileHandle.write(msg)
    return "received data"

@app.route("/logout")
def logout():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        session['logged_in'] = False
        session['logout_time'] = datetime.datetime.now()
        updateUserSessionInfo(session['current_user'], session['login_time'], session['logout_time'], session['eventfilename'])
        global logFileHandle
        if logFileHandle:
            logFileHandle.close()
        return home()


def updateUserSessionInfo(username, loginTime, logoutTime, eventFileName):
    if username and loginTime and logoutTime and eventFileName:
        Session = sessionmaker(bind=engine)
        session = Session()
        infoObj = SessionInfo(username, loginTime, logoutTime)
        session.add(infoObj)
        session.commit()

        eventInfoObj = EventInfo(username, loginTime, eventFileName)
        session.add(eventInfoObj)
        session.commit()
    return


if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True, host='0.0.0.0', port=4000)
