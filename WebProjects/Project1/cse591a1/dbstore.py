import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database import *
 
engine = create_engine('sqlite:///tutorial.db', echo=True)

Base = declarative_base()

########################################################################
class EventInfo(Base):
    """"""
    __tablename__ = "eventinfo"

    id = Column(Integer, primary_key=True)
    username = Column(String, ForeignKey("users.username"))
    logintime = Column(DateTime)
    eventfilename = Column(String)

    # ----------------------------------------------------------------------
    def __init__(self, username, logintime, eventfilename):
        """"""
        self.username = username
        self.logintime = logintime
        self.eventfilename = eventfilename


# create tables
Base.metadata.create_all(engine)



# # create a Session
# Session = sessionmaker(bind=engine)
# session = Session()
#
# user = User("aaa","123")
# session.add(user)
# session.commit()
#
# user = User("bbb","123")
# session.add(user)
# session.commit()
#
# user = User("ccc","123")
# session.add(user)
# session.commit()