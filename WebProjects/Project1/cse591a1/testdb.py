import os
from sqlalchemy.orm import sessionmaker
from database import *
from sqlalchemy import MetaData

engine = create_engine('sqlite:///tutorial.db', echo=True)

# result = engine.execute('SELECT * FROM '
#                         '"sessioninfo"')
# result1 = engine.execute('SELECT * FROM '
#                                 '"sessioninfo" WHERE username = '+'aaa')
# Session = sessionmaker(bind=engine)
#     s = Session()
#     query = s.query(User).filter(User.username.in_([POST_USERNAME]), User.password.in_([POST_PASSWORD]))
#     result = query.first()
# if result1:
#     for _r in result1:
#         print _r


# m = MetaData()
# m.reflect(engine)
# for table in m.tables.values():
#     print(table.name)
#     for column in table.c:
#         print(column.name)
Session = sessionmaker(bind=engine)
s = Session()
query = s.query(SessionInfo).filter(SessionInfo.username.in_(['aaa']))
sessionInfoItems = query.all()
if sessionInfoItems:
    for _r in sessionInfoItems:
        print _r.username
        print _r.logintime
        print _r.logouttime

print 'done'