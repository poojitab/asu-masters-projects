import os
from sqlalchemy.orm import sessionmaker
from database import *
from sqlalchemy import MetaData

engine = create_engine('sqlite:///tutorial.db', echo=True)
Session = sessionmaker(bind=engine)
s = Session()
# rows  = s.query(User).delete();
# s.commit()
# rows = s.query(SessionInfo).delete();
# s.commit()
rows = s.query(UserEvents).delete();
s.commit()
print 'cleanup completed'