from sqlalchemy import *
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

engine = create_engine('sqlite:///tutorial.db', echo=True)
Base = declarative_base()


########################################################################
class User(Base):
    """"""
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)

    # ----------------------------------------------------------------------
    def __init__(self, username, password):
        """"""
        self.username = username
        self.password = password


class SessionInfo(Base):
    """"""
    __tablename__ = "sessioninfo"

    id = Column(Integer, primary_key=True)
    username = Column(String, ForeignKey("users.username"))
    logintime = Column(DateTime)
    logouttime = Column(DateTime)

    # ----------------------------------------------------------------------
    def __init__(self, username, logintime, logouttime):
        """"""
        self.username = username
        self.logintime = logintime
        self.logouttime = logouttime

class EventInfo(Base):
    """"""
    __tablename__ = "eventinfo"

    id = Column(Integer, primary_key=True)
    username = Column(String, ForeignKey("users.username"))
    logintime = Column(DateTime)
    eventfilename = Column(String)

    # ----------------------------------------------------------------------
    def __init__(self, username, logintime, eventfilename):
        """"""
        self.username = username
        self.logintime = logintime
        self.eventfilename = eventfilename

class UserEvents(Base):
    """"""
    __tablename__ = "userevents"

    id = Column(Integer, primary_key=True)
    username = Column(String, ForeignKey("users.username"))
    eventtime = Column(DateTime)
    event = Column(String)

    # ----------------------------------------------------------------------
    def __init__(self, username, eventtime, event):
        """"""
        self.username = username
        self.eventtime = eventtime
        self.event = event

# create tables
Base.metadata.create_all(engine)