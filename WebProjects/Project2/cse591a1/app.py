from __future__ import division
from flask import Flask
from flask import Flask, flash, redirect, render_template, request, session, abort
import os, datetime, uuid, collections
from sqlalchemy.orm import sessionmaker
from database import *
import json

engine = create_engine('sqlite:///tutorial.db', echo=True)

app = Flask(__name__)

usersWorkingHoursEvents = {}
usersNonWorkingHoursEvents = {}
allUsersWorkingHoursEvents = {}
allUsersNonWorkingHoursEvents = {}


@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        Session = sessionmaker(bind=engine)
        s = Session()
        query1 = s.query(SessionInfo).filter(SessionInfo.username.in_([session.get('current_user')]))
        sessionInfoItems = query1.all()
        userEventItemsInDb = engine.execute('SELECT "event", COUNT(*) AS "COUNT" FROM "userevents" WHERE username = "' + session.get('current_user') + '" GROUP BY "event"')
        allUserEventsInDb = engine.execute('SELECT "event", COUNT(*) AS "COUNT" FROM "userevents" GROUP BY "event"')
        userEventItemsForTimeGraph = engine.execute(
            'SELECT "event", "eventtime" FROM "userevents" WHERE username = "' + session.get('current_user') + '"')
        alluserEventItemsForTimeGraph = engine.execute('SELECT "event", "eventtime" FROM "userevents"')
        allEvents = engine.execute('SELECT distinct "event" FROM "userevents"')

        noOfUsers = s.query(func.count(User.id)).scalar()
        UserEventTupleAvg = collections.namedtuple('UserEventTupleAvg', 'event current_usercount avg_count')
        UserEventTupleItem = collections.namedtuple('UserEventTupleItem', 'event COUNT')
        allUserEventsAvg = []
        allUserEvents = []
        userEventItems = []

        dict = {}

        morningTimeCutOff = datetime.time(9, 0, 0)
        eveningTimeCutOff = datetime.time(17, 0, 0)

        global usersWorkingHoursEvents, usersNonWorkingHoursEvents, allUsersWorkingHoursEvents, allUsersNonWorkingHoursEvents

        for uniqueEvent in allEvents:
            usersWorkingHoursEvents[uniqueEvent.event] = 0
            usersNonWorkingHoursEvents[uniqueEvent.event] = 0
            allUsersWorkingHoursEvents[uniqueEvent.event] = 0
            allUsersNonWorkingHoursEvents[uniqueEvent.event] = 0

        for eventItem in userEventItemsForTimeGraph:
            eventName = eventItem.event
            datetimeString = eventItem.eventtime
            datetimeString = datetimeString.replace(' ', 'T')
            datetimeString = datetimeString + 'Z'
            time = datetime.datetime.strptime(datetimeString, '%Y-%m-%dT%H:%M:%S.%fZ').time()
            if time >= morningTimeCutOff and time <= eveningTimeCutOff:
                usersWorkingHoursEvents[eventName] = int(usersWorkingHoursEvents[eventName]) + 1
            else:
                usersNonWorkingHoursEvents[eventName] = int(usersNonWorkingHoursEvents[eventName]) + 1

        for allusereventItem in alluserEventItemsForTimeGraph:
            eventName = allusereventItem.event
            datetimeString = allusereventItem.eventtime
            datetimeString = datetimeString.replace(' ', 'T')
            datetimeString = datetimeString + 'Z'
            time = datetime.datetime.strptime(datetimeString, '%Y-%m-%dT%H:%M:%S.%fZ').time()
            if time >= morningTimeCutOff and time <= eveningTimeCutOff:
                allUsersWorkingHoursEvents[eventName] = int(allUsersWorkingHoursEvents[eventName]) + 1
            else:
                allUsersNonWorkingHoursEvents[eventName] = int(allUsersNonWorkingHoursEvents[eventName]) + 1

        for userEvent in userEventItemsInDb:
            eventName = userEvent.event
            eventCount = userEvent.COUNT
            u_eventItem = UserEventTupleItem(event=eventName, COUNT=str(eventCount))
            userEventItems.append(u_eventItem)
            dict[eventName] = eventCount


        for eventItem in allUserEventsInDb:
            eventName = eventItem.event
            eventCount = eventItem.COUNT
            allUserEventItem = UserEventTupleItem(event=eventName, COUNT=str(eventCount))
            allUserEvents.append(allUserEventItem)

        for item in allUserEvents:
            eventName = item.event
            dictValue = 0
            if eventName in dict:
                dictValue = dict[eventName]
            current_usercount = int(dictValue)
            avg_count = int(item.COUNT) / noOfUsers
            u_avg_item = UserEventTupleAvg(event=eventName, current_usercount=str(current_usercount), avg_count=str(avg_count))
            allUserEventsAvg.append(u_avg_item)


    return render_template('userprofile.html', sessionInfoItems=sessionInfoItems, userEventItems=userEventItems, allUserEvents=allUserEvents, allUserEventsAvg=allUserEventsAvg)

@app.route('/getTimeGraphDataForUser', methods=['POST'])
def get_data_for_userTimeGraph():
    global usersWorkingHoursEvents, usersNonWorkingHoursEvents
    data = str(request.get_data()).strip()

    print 'data in getTimeGraphDataForUser : |' + data + '|'

    googleGraphData = json.dumps({"cols":[{"id":"", "label":"Events","type":"string"},
                                          {"id":"", "label":"No of Events", "type":"number"}],
                                  "rows":[
                                      {"c":[{"v":"Working Hours", "f":"plc"},{"v": usersWorkingHoursEvents[data],"f":"plc"}]},
                                      {"c":[{"v":"Non-Working Hours","f":"plc"},{"v": usersNonWorkingHoursEvents[data],"f":"plc"}]}
                                  ]}, sort_keys=false)

    googleGraphData = googleGraphData.replace("\"plc\"","null")
    return googleGraphData

@app.route('/getTimeGraphDataForGroup', methods=['POST'])
def get_data_for_GroupTimeGraph():
    global allUsersWorkingHoursEvents, allUsersNonWorkingHoursEvents
    data = str(request.get_data()).strip()

    print 'data in getTimeGraphDataForGroup : |' + data + '|'

    googleGraphData = json.dumps({"cols":[{"id":"", "label":"Events","type":"string"},
                                          {"id":"", "label":"No of Events", "type":"number"}],
                                  "rows":[
                                      {"c":[{"v":"Working Hours", "f":"plc"},{"v":allUsersWorkingHoursEvents[data],"f": "plc"}]},
                                      {"c":[{"v":"Non-Working Hours","f":"plc"},{"v":allUsersNonWorkingHoursEvents[data],"f": "plc"}]}
                                  ]}, sort_keys=false)

    googleGraphData = googleGraphData.replace("\"plc\"","null")
    return googleGraphData


@app.route('/login', methods=['POST'])
def do_admin_login():
    POST_USERNAME = str(request.form['username'])
    POST_PASSWORD = str(request.form['password'])
    Session = sessionmaker(bind=engine)
    s = Session()
    query = s.query(User).filter(User.username.in_([POST_USERNAME]), User.password.in_([POST_PASSWORD]))
    result = query.first()
    if result:

        session['logged_in'] = True
        session['current_user'] = POST_USERNAME
        session['login_time'] = datetime.datetime.now()




    else:
        flash('wrong password!or user does not exist!')
    return home()

@app.route('/signup', methods=['GET', 'POST'])
def do_register_user():
    session['user_creation_status_message'] = ""
    if request.method == 'POST':
        POST_USERNAME = str(request.form['username'])
        POST_PASSWORD = str(request.form['password'])
        POST_CONFIRM_PASSWORD = str(request.form['confirm-password'])
        if POST_USERNAME and POST_PASSWORD and POST_CONFIRM_PASSWORD:
            if POST_PASSWORD == POST_CONFIRM_PASSWORD:
                Session = sessionmaker(bind=engine)
                s = Session()
                query = s.query(User).filter(User.username.in_([POST_USERNAME]))
                result = query.first()
                if result:
                    session['user_creation_status_message'] = 'Sorry user already exists ! Please Retry'
                else:
                    userObj = User(POST_USERNAME, POST_PASSWORD)
                    s.add(userObj)
                    s.commit()
                    session['user_creation_status_message'] = 'User created successfully ! Please proceed to login page'
            else:
                session['user_creation_status_message'] = 'Passwords do not match! Please Retry'
        else:
            session['user_creation_status_message'] = 'Missing fields. All fields are mandatory!'

    return render_template('signup.html')


@app.route('/logdata', methods=['POST'])
def do_log_data():
    data = str(request.get_data())
    Session = sessionmaker(bind=engine)
    s = Session()
    infoObj = UserEvents(session.get('current_user'),datetime.datetime.now(), data)
    s.add(infoObj)
    s.commit()

    return "received data"

@app.route("/logout")
def logout():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        session['logged_in'] = False
        session['logout_time'] = datetime.datetime.now()
        updateUserSessionInfo(session['current_user'], session['login_time'], session['logout_time'])
        return home()


def updateUserSessionInfo(username, loginTime, logoutTime):
    if username and loginTime and logoutTime:
        Session = sessionmaker(bind=engine)
        session = Session()
        infoObj = SessionInfo(username, loginTime, logoutTime)
        session.add(infoObj)
        session.commit()


    return


if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True, host='0.0.0.0', port=4000)