CSE 591: Adaptive Web - Assignment 2
Poojita Badarinarayan

Video Link: https://www.youtube.com/watch?v=bzBLKaES1HA&feature=youtu.be

Assignment 2 info:

-Google charts has been made use of for the entire assignment's visualizations
-Bonus has been attempted
-All events except Scroll has been taken into account (More than 3) for visualizations
-3 interactions have been successfully captured. More info on that can be found below
- The detailed analysis and patterns have been explained on the website.

-This assignment has 3 different types of interactions that has been captured:

1)"Drop down menu" to select kind of graph from
  This has 2 options (Social visualization of all events, contributions data). Social visualization of all events gives a basic idea of event distribution both for current user and all users in group. Contributions data gives more information about individual contributions based on average computed. System average per event is computed as "(Total number of a particular event occurences)/(Number of users registered in system)"
	
2)"Mouse hover" adds more value to pie graphs's information
  This options gives the individual event count for both users and group on mouse hover.


3)"Click" on individual events on prior graph to get more information
  Click on any event either on pie graph or bar graph (For color blind people). This gives information on when the occurences of the event was performed (Working or non working hours)

BONUS:
-This assignment also has a provision for color blind users. 
-A text blinker is added to indicate all the users about the same. In case he/she is color blinded they will know what exactly to do next!
-The polychromatic pie graph may not be easily perceived by color blind people due to the nature of its conditions.
-Such users can use the drop down menu to select monochromatic graph which displays the pie chart as bar graph with a single color. This is more easily understandable and appreciated by them.
- Such a website can satisfy a wider range of users (Both non color blinded and color blinded people)

References:
[1] https://developers.google.com/chart/
