var htmlString = $('body').html().toString();
var pageContainsJavaTag = (htmlString.indexOf("/tagged/java") > -1);
var isStackoverflowpage = (window.location.href.indexOf("stackoverflow.com") > -1);

$(".vote-up-off").click(function() {
   if(pageContainsJavaTag)
   {
      $.post( "http://localhost:4000/logdata", "Upvote Event" );
   }
});

$("#addInterestingTag").click(function() {
   if(pageContainsJavaTag)
   {
      $.post( "http://localhost:4000/logdata", "Favorite tag Event" );
   }

});

$(document).ready(function(){
    $(".jobs li").click(function(){

        if(pageContainsJavaTag)
        {
            $.post( "http://localhost:4000/logdata", "Job Interest Event" );
        }

    });
});
$(".vote-down-off").click(function() {
   if(pageContainsJavaTag)
   {
      $.post( "http://localhost:4000/logdata", "Downvote Event" );
   }
});

$(".short-link").click(function() {
   if(pageContainsJavaTag)
   {
      //alert('Share Stack Overflow clicked' );
      $.post( "http://localhost:4000/logdata", "Page shared");
   }
});

$(".btn").click(function() {
   if(pageContainsJavaTag)
   {
      //alert('Ask a question button clicked' );
      $.post("http://localhost:4000/logdata", "Ask Question Event");
   }
});

$("#submit-button").click(function() {
   if(pageContainsJavaTag)
   {
      //alert('Post your answer button clicked' );
      $.post("http://localhost:4000/logdata", "Answer post Event");
   }
});

$(window).load(function() {
  if (isStackoverflowpage && pageContainsJavaTag)
  {
	  var currentURL = ""+window.location.href;
	  $.post( "http://localhost:4000/logdata", "Page with JAVA tag loaded" );
  }
});



$( window ).on('beforeunload',function() {
   if (isStackoverflowpage && pageContainsJavaTag)
   {
      var currentURL = ""+window.location.href;
      $.post( "http://localhost:4000/logdata", "Page Navigated away" );
   }
});